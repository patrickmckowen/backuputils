// Copyright © 2019 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"log"
	"os"
	"bufio"
	"os/exec"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

)

// runCmd represents the run command
var runCmd = &cobra.Command{
	Use:   "run",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("run called")



		// Try to find the default output file in config file
		source := viper.GetString("source")
		if source != ""{
			log.Println("\"source\" found in config")
			log.Println("source is set by config file to ", source)
		}

		// look for output file in CLI ARGS (WILL OVERRIDE)
		clisource, _ := cmd.Flags().GetString("source")
		if clisource != "" {	// If the value is empty ...
			log.Println("source specified by cli.")
			source = clisource
			log.Println("source set by cli to ", source)
		}

		// look for NAME in CONFIG FILE
		log.Println("Source final value.", source)



		// ----------------- Destination ----------------------- //

		// Try to find the default output file in config file
		destination := viper.GetString("destination")
		if destination != ""{
			log.Println("\"destination\" found in config")
			log.Println("destination is set by config file to ", destination)
		}

		// look for output file in CLI ARGS (WILL OVERRIDE)
		clidestination, _ := cmd.Flags().GetString("destination")
		if clidestination != "" {	// If the value is empty ...
			log.Println("destination file specified by cli.")
			destination = clidestination
			log.Println("destination file set by cli to ", destination)
		}

		// look for NAME in CONFIG FILE
		log.Println("Destination final value ", destination)




			// docker build current directory
	cmdName := "daily_backup"
	cmdArgs := []string{source, destination}

	command := exec.Command(cmdName, cmdArgs...)
	cmdReader, err := command.StdoutPipe()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error creating StdoutPipe for Cmd", err)
		os.Exit(1)
	}

	scanner := bufio.NewScanner(cmdReader)
	go func() {
		for scanner.Scan() {
			fmt.Printf(scanner.Text())
		}
	}()

	err = command.Start()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error starting daily_backup script", err)
		os.Exit(1)
	}

	err = command.Wait()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error waiting for daily_backup script", err)
		os.Exit(1)
	}
	},
}

func init() {
	rootCmd.AddCommand(runCmd)

	runCmd.Flags().StringP("source", "s", "", "The root directory to backup to destination.")
	runCmd.Flags().StringP("destination", "d", "", "The destination directory to put the backups.")
}

